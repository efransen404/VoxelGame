using UnityEditorInternal.Profiling.Memory.Experimental;

namespace Framework.Voxel
{
    /**
     * An immutable struct to represent data for different types of voxels
     */
    public readonly struct VoxelData
    {
        // The name of the voxel
        public readonly string Name;
        // Whether the voxel will be shown or not
        public readonly bool IsOpaque;
        // Whether the voxel will be used for collisions
        public readonly bool IsSolid;
        // The path to the texture to draw the block
        public readonly string TexturePath;

        public VoxelData(string name, bool isOpaque, bool isSolid, string texturePath)
        {
            Name = name;
            IsOpaque = isOpaque;
            IsSolid = isSolid;
            TexturePath = texturePath;
        }
    }
}
