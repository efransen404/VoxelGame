
// Unity includes
using Unity.Collections;
using Unity.Entities;

namespace Framework.Voxel
{
    /// <Summary> A simplified struct representation of VoxelRegistrySystem to pass to jobs </summary>
    /// 
    public struct VoxelRegistry
    {
        // A mapping from names to IDs
        // Using special Unity types that can be passed to jobs
        private NativeHashMap<FixedString32, int> _nameToIDMap;
        private NativeList<VoxelData> _voxelData;

        /// <summary>
        /// A method to add voxel data to the registry
        /// </summary>
        /// <param name="voxel"> The VoxelData to be added</param>
        /// <returns> True if the voxel was successfully added, false otherwise</returns>
        public bool RegisterVoxel(VoxelData voxel)
        {
            // First we check if the voxel is already registered
            if (_nameToIDMap.ContainsKey(voxel.Name)) return false;
            // Now we try to add the name to ID pairing to the map
            if (!_nameToIDMap.TryAdd(voxel.Name, _voxelData.Length)) return false;
            // Finally we add the voxel to the list
            _voxelData.Add(voxel);
            return true;
        }
 
        // A method to access the internal string to id mapping
        public int GetID(FixedString32 s)
        {
            return _nameToIDMap[s];
        }
 
        // A method to access the internal id to VoxelData mapping
        public VoxelData? GetVoxel(int id)
        {
            if (id < 0 || id >= _voxelData.Length) return null;
            else return _voxelData[id];
        }
 
        // A method to get VoxelData from a string
        public VoxelData GetVoxel(FixedString32 s)
        {
            return _voxelData[_nameToIDMap[s]];
        }

        public NativeArray<VoxelData> GETVoxels()
        {
            return _voxelData.AsArray();
        }

        // A method to check if the internal string to id mapping contains a given name
        public bool Contains(FixedString32 s)
        {
            return _nameToIDMap.ContainsKey(s);
        }

        // A method to destroy the internal native maps, as they are not memory managed
        public void Dispose()
        {
            _nameToIDMap.Dispose();
            _voxelData.Dispose();
        }
    }
    public class VoxelRegistrySystem : SystemBase
    {
        public VoxelRegistry Registry { get; private set; }

        /// <summary>
        /// Unity method called when the system is created
        /// </summary>
        protected override void OnCreate()
        {
            base.OnCreate();
            Registry = new VoxelRegistry();
        }

        /// <summary>
        /// Method used to add a new voxel to the registry
        /// </summary>
        /// <param name="voxel"> VoxelData to be added</param>
        /// <returns> True if added successfully, false otherwise</returns>
        public bool RegisterVoxel(VoxelData voxel)
        {
            return Registry.RegisterVoxel(voxel);
        }

        protected override void OnUpdate()
        {

        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Registry.Dispose();
        }
    }


}