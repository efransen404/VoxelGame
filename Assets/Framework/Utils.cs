using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace Framework
{
    public static class Utils
    {
        /// <summary>
        /// Size of the X dimension of chunks by voxel
        /// </summary>
        public const int ChunkX = 16;
        /// <summary>
        /// Size of the Y dimension of chunks by voxel
        /// </summary>
        public const int ChunkY = 16;
        /// <summary>
        /// Size of the Z dimension of chunks by voxel
        /// </summary>
        public const int ChunkZ = 16;
        /// <summary>
        /// Volume of the chunk in voxels^3
        /// </summary>
        public const int ChunkVolume = ChunkX * ChunkY * ChunkZ;

        /// <summary>
        /// A 3D Vector representation of the chunk size in terms of voxels
        /// </summary>
        public static readonly int3 ChunkSize = new int3(ChunkX, ChunkY, ChunkZ);

        /// <summary>
        /// Size of the X dimension of regions in chunks
        /// </summary>
        public const int RegionX = 4;
        /// <summary>
        /// Size of the Y dimension of regions in chunks
        /// </summary>
        public const int RegionY = 4;
        /// <summary>
        /// Size of the Z dimension of regions in chunks
        /// </summary>
        public const int RegionZ = 4;
        /// <summary>
        /// Volume of the region in cubic chunks
        /// </summary>
        public const int RegionVolume = RegionX * RegionY * RegionZ;
        public static readonly int3 RegionSize = new int3(RegionX, RegionY, RegionZ);

        /// <summary>
        /// A function used to convert 3 indexes (like a 3D array) to a single index for a 1D array
        /// </summary>
        /// <param name="x">X coord of the index</param>
        /// <param name="y">Y coord of the index</param>
        /// <param name="z">Z coord of the index</param>
        /// <returns>The 1D representation of the index</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int PosToIndex(int x, int y, int z) => x + ChunkX * (y + ChunkY * z);

        /// <summary>
        /// A function used to convert a 3D Vector to represent a position in a 3D array to a single index
        /// </summary>
        /// <param name="p">The vector for the point</param>
        /// <returns>The 1D array index</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int PosToIndex(int3 p) => PosToIndex(p.x, p.y, p.z);

        /// <summary>
        /// A function used to convert a 3D position vector and 2D size vector to a single index in a 3D array
        /// </summary>
        /// <param name="p">3D Position vector</param>
        /// <param name="sizeXY">2D size vector to represent the X and Y dimension of a 3D space</param>
        /// <returns>1D array index representation</returns>
        public static int PosToIndex(int3 p, int2 sizeXY) => p.x + sizeXY.x * (p.y + sizeXY.y * p.z);

        /// <summary>
        /// A function to convert a single index from a 1D array to a 3D position vector
        /// </summary>
        /// <param name="i">1D index</param>
        /// <returns>3D Position Vector</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int3 IndexToPos(int i)
        {
            var z = i / (ChunkX * ChunkY);
            var y = (i - z * ChunkX * ChunkY) / ChunkX;
            var x = i - ChunkX * (y + ChunkY * z);

            return new int3(x, y, z);
        }

        /// <summary>
        /// 3D Vector representation for the normals of the 6 faces of a cube
        /// </summary>
        public static readonly int3[] Directions = new int3[]
        {
            new int3(0, 1, 0),  // Up
            new int3(0, -1, 0), // Down
            new int3(-1, 0, 0), // Left
            new int3(1, 0, 0),  // Right
            new int3(0, 0, 1),  // Forward
            new int3(0, 0, -1)  // Back
        };
    }
}