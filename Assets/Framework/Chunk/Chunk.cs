
using Unity.Entities;
using Unity.Mathematics;

namespace Framework.Chunk
{
    /// <summary>
    /// A Unity ECS component that stores the voxel data for a chunk.
    /// </summary>
    /// <remarks>
    /// Has a default volume of <see cref="Utils.ChunkVolume"/> and generates a game object for editing values
    /// </remarks>
    [InternalBufferCapacity(Utils.ChunkVolume)]
    [System.Serializable]
    [GenerateAuthoringComponent]
    public struct ChunkVoxelType : IBufferElementData
    {
        /// <summary>
        /// The type of each voxel, from 0 to 65,535 for 65,535 different voxel types
        /// </summary>
        public ushort voxelType;
        public static implicit operator ChunkVoxelType(int i) => new ChunkVoxelType() { voxelType = (ushort)i };
        public static implicit operator int(ChunkVoxelType c) => c.voxelType;
    }

    [System.Serializable]
    public struct ChunkPosition : IComponentData
    {
        public int3 position;
        
        public int X => position.x;

        public int Y => position.y;

        public int Z => position.z;

        public static implicit operator int3(ChunkPosition pos) => pos.position;
    }
}
