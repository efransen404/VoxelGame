using Unity.Entities;

namespace Framework.Chunk
{
    // A component that doesn't contain any data that is used to mark chunks that need meshes generated
    public struct GenerateMesh : IComponentData
    {
        
    }
}