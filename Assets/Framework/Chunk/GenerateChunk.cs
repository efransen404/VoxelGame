using Unity.Entities;

namespace Framework.Chunk
{
    /// <summary>
    /// A component used to mark that a chunk needs to be generated
    /// </summary>
    public struct GenerateChunk : IComponentData
    {
        
    }
}