using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Framework.Chunk
{
    public class ChunkGenerationSystem : SystemBase
    {
        // An Entity Command Buffer system, used to hold a list of commands to update in a frame
        // EndSimulation runs at the end of the simulation phase of the unity update cycle
        private EndSimulationEntityCommandBufferSystem _esecb;

        /// <summary>
        /// A function pointer to handle chunk generation for voxels.
        /// </summary>
        /// <seealso cref="ChunkGenerator.GenerateVoxel"/>
        public FunctionPointer<ChunkGenerator.GenerateVoxel> GenerateVoxelPointer =
            BurstCompiler.CompileFunctionPointer<ChunkGenerator.GenerateVoxel>(ChunkGenerator.Default);

        public FunctionPointer<ChunkGenerator.GenerateVoxel4> GenerateVoxel4Pointer =
            BurstCompiler.CompileFunctionPointer<ChunkGenerator.GenerateVoxel4>(ChunkGenerator.Default4);
        
        
    
        protected override void OnCreate()
        {
            base.OnCreate();
            // Tells the Unity engine to either get or create the buffer system
            _esecb = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            // Gets a command buffer from the End Simulation Entity Command Buffer System
            EntityCommandBuffer commandBuffer = _esecb.CreateCommandBuffer();
            FunctionPointer<ChunkGenerator.GenerateVoxel> generateVoxelPointer = GenerateVoxelPointer;

            // Start an entity query
            Entities
                    // Gives it this description
                .WithName("AddVoxelBuffersToChunks")
                    // Gets all chunk with a GenerateChunk component
                .WithAll<GenerateChunk>()
                    // Makes sure they don't have the ChunkVoxelType buffer
                .WithNone<ChunkVoxelType>()
                    // Calls this lambda for each entity
                .ForEach((Entity e) =>
                {
                    // Adds a ChunkVoxelType buffer to the entity
                    commandBuffer.AddBuffer<ChunkVoxelType>(e);
                }).ScheduleParallel();

            // Start another query
            Entities
                .WithName("GenerateChunk")
                // Gets entities with both GenerateChunk and ChunkVoxelType buffer
                .WithAll<GenerateChunk>()
                .ForEach((Entity e, ref DynamicBuffer<ChunkVoxelType> voxelBuffer, in ChunkPosition chunkPosition) =>
                {
                    voxelBuffer.ResizeUninitialized(Utils.ChunkVolume);
                    var voxels = voxelBuffer.AsNativeArray();

                    for (int x = 0; x < Utils.ChunkSize.x; x++)
                    {
                        for (int y = 0; y < Utils.ChunkSize.y; y++)
                        {
                            for (int z = 0; z < Utils.ChunkSize.z; z++)
                            {
                                int3 offset = chunkPosition * Utils.ChunkSize;
                                int3 position = new int3(x, y, z) + offset;
                                voxels[Utils.PosToIndex(x, y, z)] = generateVoxelPointer.Invoke(ref position);
                            }
                        }
                    }
                    
                    // Remove the generateChunk component
                    commandBuffer.RemoveComponent<GenerateChunk>(e);
                    // Add a GenerateMesh component
                    commandBuffer.AddComponent<GenerateMesh>(e);
                }).ScheduleParallel();
        }

        private void GenerateChunk(EntityCommandBuffer commandBuffer, FunctionPointer<ChunkGenerator.GenerateVoxel> generateVoxelPointer)
        {
            Entities
                .WithName("GenerateChunkBy1")
                .WithAll<GenerateChunk>()
                .ForEach((Entity e, ref DynamicBuffer<ChunkVoxelType> voxelBuffer, in ChunkPosition chunkPosition) =>
                {
                    voxelBuffer.ResizeUninitialized(Utils.ChunkVolume);
                    NativeArray<ChunkVoxelType> voxels = voxelBuffer.AsNativeArray();

                    for (int x = 0; x < Utils.ChunkSize.x; x++)
                    {
                        for (int y = 0; y < Utils.ChunkSize.y; y++)
                        {
                            for (int z = 0; z < Utils.ChunkSize.z; z++)
                            {
                                int3 offset = chunkPosition * Utils.ChunkSize;
                                int3 position = new int3(x, y, z) + offset;
                                voxels[Utils.PosToIndex(x, y, z)] = generateVoxelPointer.Invoke(ref position);
                            }
                        }
                    }
                    
                    // Remove the generateChunk component
                    commandBuffer.RemoveComponent<GenerateChunk>(e);
                    // Add a GenerateMesh component
                    commandBuffer.AddComponent<GenerateMesh>(e);
                }).ScheduleParallel();
        }

        private void Generate4Chunks(EntityCommandBuffer commandBuffer,
            FunctionPointer<ChunkGenerator.GenerateVoxel4> generateVoxelPointer)
        {
            Entities
                .WithName("GenerateChunksX4")
                .WithAll<GenerateChunk>()
                .ForEach((Entity e, ref DynamicBuffer<ChunkVoxelType> voxelBuffer, in ChunkPosition chunkPosition) =>
                {
                    voxelBuffer.ResizeUninitialized(Utils.ChunkVolume);
                    NativeArray<ChunkVoxelType> voxels = voxelBuffer.AsNativeArray();

                    for (int4 x = new int4(0, 1, 2, 3); x[3] < Utils.ChunkSize.x; x++)
                    {
                        for (int4 y = new int4(0, 1, 2, 3); y[3] < Utils.ChunkSize.y; y++)
                        {
                            for (int4 z = new int4(0, 1, 2, 3); z[3] < Utils.ChunkSize.z; z++)
                            {
                                int3 offset = chunkPosition * Utils.ChunkSize;
                                int4 posX = x + offset.x;
                                int4 posY = y + offset.y;
                                int4 posZ = z + offset.z;
                                uint4 result = new uint4();
                                generateVoxelPointer.Invoke(ref posX, ref posY, ref posZ, ref result);
                                int index = Utils.PosToIndex(x[0], y[0], z[0]);
                                voxels[index] = (ushort)result[0];
                                voxels[index + 1] = (ushort)result[1];
                                voxels[index + 2] = (ushort)result[2];
                                voxels[index + 3] = (ushort)result[3];
                            }
                        }
                    }

                    commandBuffer.RemoveComponent<GenerateChunk>(e);
                    commandBuffer.AddComponent<GenerateMesh>(e);
                }).ScheduleParallel();
        }
    }

    [BurstCompile]
    public class ChunkGenerator
    {
        /// <summary>
        /// A method for generating voxel data for a chunk
        /// Takes a x, y, and z world position and returns a ushort voxel index
        /// </summary>
        public delegate ushort GenerateVoxel(ref int3 pos);

        /// <summary>
        /// A method for generating voxel data for a chunk using 4 wide data types for SIMD support
        /// </summary>
        public delegate void GenerateVoxel4(ref int4 x, ref int4 y, ref int4 z, ref uint4 output);

        /// <summary>
        /// The default method for generating voxels. Creates a flat world at level 10.
        /// </summary>
        /// <param name="pos">The world position</param>
        /// <returns>0 if pos.y is greater than 10, 1 otherwise.</returns>
        [BurstCompile]
        public static ushort Default(ref int3 pos)
        {
            return (ushort)(pos.y > 10 ? 0 : 1); 
        }

        [BurstCompile]
        public static void Default4(ref int4 x, ref int4 y, ref int4 z, ref uint4 output)
        {
            bool4 fields = y <= 10;
            output = new uint4(fields);
        }
    }
}
