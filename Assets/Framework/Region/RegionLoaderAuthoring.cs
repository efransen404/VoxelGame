using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Framework.Region
{
    /// <summary>
    /// An ECS component used to mark an entity as a region loader, and has a range and a region index
    /// </summary>
    public struct RegionLoader : IComponentData
    {
        public int Range;
        public int2 RegionIndex;
    }
    
    /// <summary>
    /// A class used by Unity to create a gameobject that can modify the RegionLoader component on an entity
    /// </summary>
    public class RegionLoaderAuthoring
    {
        [SerializeField] private int _range = 5;

        public void Convert(Entity entity, EntityManager manager, GameObjectConversionSystem conversionSystem)
        {
            manager.AddComponentData(entity, new RegionLoader
            {
                Range = _range
            });
        }
    }
}