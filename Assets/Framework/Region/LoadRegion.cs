using Unity.Entities;

namespace Framework.Region
{
    /// <summary>
    /// An empty ECS component to flag a <see cref="Region"/>that needs to be loaded
    /// </summary>
    public struct LoadRegion : IComponentData
    {
        
    }
}