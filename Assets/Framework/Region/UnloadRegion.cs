using Unity.Entities;

namespace Framework.Region
{
    /// <summary>
    /// An empty ECS component used to flag a <see cref="Region"/> that needs unloaded
    /// </summary>
    public struct UnloadRegion : IComponentData
    {
        
    }
}
