using Framework.Chunk;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;

namespace Framework.Region
{
    public class RegionGenerateSystem :SystemBase
    {
        private static readonly int3 RegionSize = Utils.RegionSize;
        private static readonly int3 ChunkSize = Utils.ChunkSize;
        private EndInitializationEntityCommandBufferSystem _ecbs;

        protected override void OnCreate()
        {
            base.OnCreate();
            _ecbs = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            GenerateRegions();
        }

        private void GenerateRegions()
        {
            EntityCommandBuffer ecb = _ecbs.CreateCommandBuffer();
            Entities
                .WithAll<GenerateRegion>()
                .ForEach((Entity r, in DynamicBuffer<LinkedEntityGroup> chunks, in RegionIndex index) =>
                {
                    // Iterate through all the chunks
                    for (int i = 0; i < chunks.Length; i++)
                    {
                        ecb.AddComponent<GenerateChunk>(chunks[i].Value);
                    }
                    // We marked all the chunks in the region for generation, so now we remove the generation flag for the region
                    ecb.RemoveComponent<GenerateRegion>(r);
                }).Schedule();
        }
    }
}
