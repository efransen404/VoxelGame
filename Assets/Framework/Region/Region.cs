using Unity.Entities;
using Unity.Mathematics;

namespace Framework.Region
{
    /// <summary>
    /// An empty ECS component to flag an entity as a Region
    ///
    /// Used by <see cref="RegionGenerateSystem"/>, <see cref="RegionLoaderSystem"/>, and <see cref="RegionLoadUnloadSystem"/>
    /// </summary>
    /// <seealso cref="GenerateRegion"/>
    /// <seealso cref="UnloadRegion"/>
    /// <seealso cref="LoadRegion"/>
    /// <seealso cref="RegionIndex"/>
    public struct Region : IComponentData
    {
        
    }
}
