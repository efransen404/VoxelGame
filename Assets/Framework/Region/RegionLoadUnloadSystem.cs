using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Framework.Region
{
    /// <summary>
    /// An ECS system to handle loading regions from and saving regions to files
    /// </summary>
    /// <seealso cref="Region"/>
    /// <seealso cref="RegionLoaderSystem"/>
    public class RegionLoadUnloadSystem : SystemBase
    {
        private NativeHashSet<int2> _savedRegions;
        private EndInitializationEntityCommandBufferSystem _ecbSystem;
        protected override void OnCreate()
        {
            base.OnCreate();
            _savedRegions = new NativeHashSet<int2>(100, Allocator.Persistent);
            _ecbSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
            // TODO: Read world data file to see what chunks are loaded
        }
        protected override void OnUpdate()
        {
            var ecb = _ecbSystem.CreateCommandBuffer();

            Entities
                .WithAll<LoadRegion>()
                .WithoutBurst()
                .ForEach((Entity e, in RegionIndex index) =>
                {
                    if (_savedRegions.Contains(index))
                    {
                        // TODO: Load the region from a file
                    }
                    else
                    {
                        // Flag that the region needs to be generated and pass it off to the generator
                        ecb.AddComponent<GenerateRegion>(e);
                        ecb.RemoveComponent<LoadRegion>(e);
                    }

                }).Run();
            Entities
                .WithAll<UnloadRegion>()
                .WithoutBurst()
                .ForEach((Entity e, in RegionIndex index) =>
                {
                    // TODO: save region to file
                    _savedRegions.Add(index);
                    ecb.DestroyEntity(e);

                }).Run();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _savedRegions.Dispose();
        }
    }
}