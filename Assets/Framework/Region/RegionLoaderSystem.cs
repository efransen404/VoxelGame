using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Framework.Chunk;

namespace Framework.Region
{
    /// <summary>
    /// An ECS system that marks regions for loading, unloading, and generating
    /// </summary>
    /// <seealso cref="Region"/>
    /// <seealso cref="RegionGenerateSystem"/>
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public class RegionLoaderSystem : SystemBase
    {
        private EndInitializationEntityCommandBufferSystem _bufferSystem;
        private EntityArchetype _regionArchetype;
        private EntityArchetype _chunkArchetype;
        private static readonly int3 RegionSize = Utils.RegionSize;

        protected override void OnCreate()
        {
            base.OnCreate();
            _bufferSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
            _regionArchetype = EntityManager.CreateArchetype(typeof(Region), typeof(RegionIndex), typeof(LoadRegion));
            _chunkArchetype = EntityManager.CreateArchetype(typeof(ChunkPosition), typeof(GenerateMesh), typeof(ChunkVoxelType));
        }

        protected override void OnUpdate()
        {
            // Get the entity command buffer so we can do all our changes at once for performance
            var ecb = _bufferSystem.CreateCommandBuffer();
            // Initialize a hashset with a starting capacity of 100 points
            NativeHashSet<int2> pointsToLoad = new NativeHashSet<int2>(100, Allocator.TempJob);
            // Iterate through all the RegionLoader entities
            Entities
                .ForEach((ref RegionLoader loader, in Translation t) =>
                {
                    // Calculate the loader's new region index
                    loader.RegionIndex = (int2)math.floor(t.Value.xz / Utils.ChunkSize.xz);
                    // Find all the points in loading range around the loader
                    GetPointsInRange(pointsToLoad, loader.RegionIndex, loader.Range);
                }).Schedule();
            // Iterate through all (loaded) Regions
            Entities
                .WithAll<Region>()
                .ForEach((Entity e, in RegionIndex regionIndexComp,
                    in DynamicBuffer<LinkedEntityGroup> linkedGroup) =>
                {
                    int2 regionIndex = regionIndexComp;
                    // If this is a region we want to load, we remove its index from the loadedPoints set
                    if (pointsToLoad.Contains(regionIndex))
                    {
                        pointsToLoad.Remove(regionIndex);
                    }
                    // Otherwise we can unload it
                    else
                    {
                        ecb.AddComponent<UnloadRegion>(e);
                    }
                }).Schedule();
            // We've checked all the loaded regions, now we need to load all the new regions
            foreach (int2 pos in pointsToLoad)
            {
                // Create a new entity with a Region tag and a RegionIndex
                Entity r = ecb.CreateEntity(_regionArchetype);
                // Set the RegionIndex to pos
                ecb.SetComponent(r, new RegionIndex { Position = pos });
                // Create a buffer for child entities, we will use it for chunks
                DynamicBuffer<LinkedEntityGroup> linkedEntities = ecb.AddBuffer<LinkedEntityGroup>(r);
                // We iterate through RegionSize
                for (var x = 0; x < RegionSize.x; x++)
                {
                    for (var y = 0; y < RegionSize.y; y++)
                    {
                        for (var z = 0; z < RegionSize.z; z++)
                        {
                            // We create a new chunk
                            Entity c = ecb.CreateEntity(_chunkArchetype);
                            // We set the chunk index
                            ecb.SetComponent(c, new ChunkPosition { position = new int3(pos.x * RegionSize.x + x, pos.y * RegionSize.y + y, z)});
                            // We add the chunk to the region
                            linkedEntities.Add(c);
                        }
                    }
                }
            }
        }

        // Gets the points in a square centered around start with sides of range * 2 size, and adds them to points
        private static void GetPointsInRange(NativeHashSet<int2> points, int2 start, int range)
        {
            for (int x = -range; x < range; ++x)
            {
                for (int z = -range; z < range; ++z)
                {
                    int2 p = start + new int2(x, z);
                    points.Add(p);
                }
            }
        }
    }
}