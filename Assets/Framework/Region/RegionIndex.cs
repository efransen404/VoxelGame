using Unity.Entities;
using Unity.Mathematics;

namespace Framework.Region
{
    /// <summary>
    /// An ECS component used to store the X and Y coordinate of a <see cref="Region"/>
    /// </summary>
    public struct RegionIndex : IComponentData
    {
        /// <summary>
        /// The X and Y position of the region
        /// </summary>
        public int2 Position;

        /// <summary>
        /// Implicit operator to convert a RegionIndex to a 2D vector
        /// </summary>
        public static implicit operator int2(RegionIndex c) => c.Position;
        /// <summary>
        /// Implicit operator to create a RegionIndex from a 2D vector
        /// </summary>
        public static implicit operator RegionIndex(int2 i) => new RegionIndex() { Position = i };

        public int X => Position.x;

        public int Y => Position.y;
    }
}