using Unity.Entities;

namespace Framework.Region
{
    /// <summary>
    /// An empty ECS component used to flag a <see cref="Region"/> that needs generated
    /// </summary>
    public struct GenerateRegion : IComponentData
    {
    
    }
}
